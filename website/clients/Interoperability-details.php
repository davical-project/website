<h1>General Information</h1>

<p>Client software will want to know several facts about the CalDAV server.
Some (like iCal and iOS) will try and discover these facts for themselves,
and others (like Thunderbird and Evolution) will require you to enter some information.</p>

<p>This means that in Evolution, Thunderbird and other software wanting a
'calendar' URL you should specify a URL which is something like:</p>
<pre>
http://calendar.example.net/caldav.php/username/calendar/
</pre>

<p>The host name is, of course, up to you.  The 'root path' should be 
<code>/caldav.php/</code> and anything following that is the calendar 
namespace.</p>

<p>Within the calendar namespace DAViCal uses the first element of the 
path as the user or 'princpal' name, so that a client connecting at the 
root path can see all of the (accessible) users and resources available 
to them (Mulberry displays this hierarchy) with any calendars below that.</p>

<p>DAViCal creates two collections automatically when a user is created.  In
recent versions these are called <b>'calendar'</b> and <b>'addressbook'</b>.  Some software
also makes it easy to create more calendars and addressbooks, or you can create
more through DAViCal's web interface, also.</p>

<p>There could well be a wider range of information about many and varied client
 software on the <a href="http://wiki.davical.org/" title="The DAViCal CalDAV Server Wiki">DAViCal Wiki</a> as well.</p>