<h1>Other Client Software</h1>

<p>I would love to have more client software available to test DAViCal
against, but so far these are the only ones I have access to.</p>

<p>If you want to point me at more free software that supports CalDAV, or
send me free copies of such proprietary software, then I will add it to
the list as well as make DAViCal work with it.</p>

<p>There could well be a wider range of information about many and varied client
 software on the <a href="http://wiki.davical.org/" title="The DAViCal CalDAV Server Wiki">DAViCal Wiki</a> as well.