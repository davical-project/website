<h1>Thunderbird (Lightning)</h1>
<p>The <a href="http://www.mozilla.org/projects/calendar/">Mozilla Calendar</a> project used offer their calendar under two different names:
 <em>Sunbird</em> was a standalone calendar application, and <em><a href="http://www.mozilla.org/projects/calendar/">Lightning</a></em> is a Thunderbird extension.
 With Sunbird being no longer actively maintained and Lightning being promoted as an official part of Thunderbird, we'll only cover the
 Lightning instructions here. You may find information on older clients on the wiki.</p>

<p>Note that Thunderbird can be extended to work with CARDDAV contacts as well. If you're interested in getting them to work, you should check out
the <a href="http://www.sogo.nu/downloads/frontends.html">SOGo Connector Thunderbird</a> extension.</p>

<ol>
<li>Select "New Calendar" from the "File" menu.</li>
<li>Choose "On the Network" (click "Next")<img src="clients/Thunderbird-dialog1.png" /> <br /> &nbsp;</li>
<li>Choose a format of "CalDAV" and enter a URL like: "http://calendar.example.net/caldav.php/username/calendar/" (click "Next")<img src="clients/Thunderbird-dialog2.png" /> <br /> &nbsp;</li>
<li>Give the calendar an appropriate display name, and choose a colour for events on this calendar. (click "Next")<img src="clients/Thunderbird-dialog3.png" /> <br /> &nbsp;</li>
<li>click "Finish"</li>
</ol>